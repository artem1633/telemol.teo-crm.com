<?php

namespace app\modules\api\controllers;

use app\models\Chat;
use app\models\DailySchedualed;
use app\models\JobTable;
use app\models\Langs;
use app\models\Money;
use app\models\MoneyTable;
use app\models\Pay;
use app\models\SetPayoutDeduction;
use app\models\Settings;
use app\models\UsersList;
use app\models\VideoMotivashki;
use app\models\WithdrawalRequests;
use app\models\Words;
use yii\rest\ActiveController;
use yii\web\Response;
use Yii;

/**
 * Default controller for the `api` module
 */
class BotinfoController extends ActiveController
{
    public $modelClass = 'app\models\Api';

    public function behaviors()
    {
        return [
            [
                'class' => 'yii\filters\ContentNegotiator',
                'only' => ['bot-in', 'testpush', 'bot-update', 'send-refer'],
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ];
    }


    /**
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionBotIn()
    {


        $content = file_get_contents('php://input'); //интересная строка: означает, что мы поточно воспринимаем запрос, который к нам пришел. Подробнее после)
        $report = json_decode($content); //интересная строка: означает, что мы поточно воспринимаем запрос, который к нам пришел. Подробнее после)
        $result = json_decode($content, true); //декодируем апдейт json, пришедший с телеграмма


//         if ($result["callback_query"]) {
//             $this->botCall($result);
//             return true;
//         }

        $text = $result["message"]["text"]; //Текст сообщения
        $chat_id = $result["message"]["chat"]["id"]; //Уникальный идентификатор пользователя
        $username = $result["message"]["chat"]["username"]; //Уникальный идентификатор пользователя
        $name = $result["message"]["from"]["first_name"]; //Юзернейм пользователя

//        $a = serialize($result);
//        self::sendTelMessage('247187885', "result {$a}");
//        return true;
        // $file = file_put_contents('income.txt', $content); // создаем текстовый файл для отладки(по желанию)

        $user_push = ['247187885'];//,'541366830'
        /** @var UsersList $user */
        $message_id = null;
        if ($result["callback_query"]) {
            $text = $result["callback_query"]['data'];
            $chat_id = $result['callback_query']['message']['chat']['id'];
            $message_id = $result['callback_query']['message']['message_id'];
//            self::sendTelMessage('247187885', "Ошибка создания {$text} - {$message_id} - {$chat_id}");
        }

        $user = UsersList::find()->where(['telegram_id' => $chat_id])->one();

//        $this->getMenu($user, '247187885', '111');


        if ($user) {
            /** @var Chat $chat */
            $chat = Chat::find()->where([])->orderBy(['id' => SORT_DESC])->one();
        }


        if ($text == '/start') {

            if ($user) {
                $user->delete();
            }

                $day = Settings::find()->where(['key' => 'register_term'])->one()->value;
                $user = new UsersList([
                    'name' => $name,
                    'telegram_id' => $chat_id,
                    'balance' => 0,
                    'partner_code' => self::genCode(),
                    'end_date' => date('Y-m-d', strtotime("+{$day} days")),
                    'status_pay' => 2,
                ]);

//                self::sendTelMessage('247187885', "Ошибка создания ".date('Y-m-d',strtotime('+5 days')));
                if (!$user->save(false)) {
                    $a = serialize($user->errors);
                    self::sendTelMessage('247187885', "Ошибка создания {$a}");
                    return true;
                }
                $langAll = Langs::find()->where(['status' => 1])->all();
                foreach ($langAll as $item) {
                    $lan[] = ["text" => $item->name, 'callback_data' => $item->id];
                }
//
                $this->getReq('sendMessage', [
                    'chat_id' => $chat_id,
                    'text' => "Select your language:\r\nВыберите язык:",
                    'reply_markup' => json_encode([
                        'inline_keyboard' => [$lan],
                    ])
                ]);

                self::newStep($user, 'step1', 0);
                return true;
        }


//        self::getWord(171, $user->language_id, $user);
        $step = $user->training_stage;
        $info = '';
        if ($text != '/start') {
            if ($step == 'step1') {
                $this->step1($user, $chat_id, $message_id, $text);
                return true;
            }
            if ($step == 'step2') {
                $this->step2($user, $chat_id, $message_id, $text);
                return true;
            }
            if ($text == 'pay') {
                $inst = self::getWord(1, $user->language_id, $user);
                $cours = self::getWord(2, $user->language_id, $user);
                $sum = self::getWord(3, $user->language_id, $user);

                $rub = Settings::find()->where(['key' => 'cours_pay'])->one()->value;
                $this->newMessage($chat_id, "{$inst} \r\n {$cours}:{$rub} \r\n {$sum}" );
                self::newStep($user, 'step48', 0);
                return true;
            }
            if ($text == 'out') {
                $inst = self::getWord(4, $user->language_id, $user);
                $cours = self::getWord(2, $user->language_id, $user);
                $sum = self::getWord(5, $user->language_id, $user);
                $rub = Settings::find()->where(['key' => 'cours_out'])->one()->value;
                $this->newMessage($chat_id, "{$inst} \r\n {$cours}:{$rub} \r\n {$sum}" );
                self::newStep($user, 'step2', 0);
                return true;
            }
            if ($text == 'info') {
                $info2 = self::getWord(6, $user->language_id, $user);
                $info3 = self::getWord(7, $user->language_id, $user);
                $this->newMessage($chat_id, $info2 );
                $this->getMenu($user, $chat_id, $info3);
                return true;
            }
            if ($text == 'lang') {
                $langAll = Langs::find()->where(['status' => 1])->all();
                foreach ($langAll as $item) {
                    $lan[] = ["text" => $item->name, 'callback_data' => $item->id];
                }
//
                $this->getReq('sendMessage', [
                    'chat_id' => $chat_id,
                    'text' => "Select your language:\r\nВыберите язык:",
                    'reply_markup' => json_encode([
                        'inline_keyboard' => [$lan],
                    ])
                ]);

                self::newStep($user, 'step1', 0);
                return true;
            }

            if ($step == 'step48') {
                $this->step48($user, $chat_id, $message_id, $text);
                return true;
            }

            if ($step == 'step49') {
                $this->step49($user, $chat_id, $message_id, $text);
                return true;
            }

            if ($step == 'step50') {
                $this->step50($user, $chat_id, $message_id, $text);
                return true;
            }
            if ($step == 'step3') {
                $this->step3($user, $chat_id, $message_id, $text);
                return true;
            }
            if ($step == 'step2') {
                $this->step2($user, $chat_id, $message_id, $text);
                return true;
            }
            if ($step == 'step4') {
                $this->step4($user, $chat_id, $message_id, $text);
                return true;
            }
            if ($step == 'step5') {
                $this->step5($user, $chat_id, $message_id, $text);
                return true;
            }

            if ($text == 'Menu' or $text == "menu") {
                if (!$info) {
                    $info = 'Menu';
                }

                $this->setClear($chat_id, $message_id);
                $this->getMenu($user, $chat_id, $info);

                return true;
            }
            if ($step == 'step47') {
                if (!$info) {
                    $info = 'Menu';
                }

                $this->setClear($chat_id, $message_id);
                $this->newMessage($chat_id, $info);

                return true;
            }
            return true;
        }

        return true;

//

    }

    /**
     * Принимает информацию о новом реферальном посещении
     * @var integer $post
     * @var integer $lang
     * @return boolean
     */
    public function getMenu($user, $chat_id, $info = "Menu")
    {
        $this->getReq('sendMessage', [
            'chat_id' => $chat_id,
            'text' => $info,
            'reply_markup' => json_encode([
                'inline_keyboard' => [
                    [
                        ['text' => self::getWord(8, $user->language_id, $user), 'callback_data' => 'pay'],
                        ['text' => self::getWord(9, $user->language_id, $user), 'callback_data' => 'out'],
                    ],
                    [
                        ['text' => self::getWord(10, $user->language_id, $user), 'callback_data' => 'info'],
                        ['text' => self::getWord(11, $user->language_id, $user), 'callback_data' => 'lang'],
                    ],
                ],
            ])
        ]);
    }




    /**
     * Принимает информацию о новом реферальном посещении
     * @var UsersList $user
     * @var integer $chat_id
     * @var integer $message_id
     * @var string $message_id
     * @return boolean
     */
    private function step5($user, $chat_id, $message_id, $text)
    {
        if ($text == '-') {

            $this->newMessage($chat_id, self::getWord(12, $user->language_id, $user));

            self::newStep($user, 'step49', 0);

            $this->setClear($chat_id, $message_id);

            return true;
        }

        if ($text == '+') {

            $with = WithdrawalRequests::find()->where(['user_id' => $user->id])->orderBy(['id' => SORT_DESC])->one();
            $with->purse = $user->users_cash;
            $with->save(false);
            $user->users_cash = false;
            $user->save(false);

            self::newStep($user, $text, 1);

            self::newStep($user, 'step47', 0);

            $id = self::getWord(13, $user->language_id, $user);
            $id2 = self::getWord(14, $user->language_id, $user);
            $this->newMessage($chat_id, "{$id} {$with->id} \r\n {$id2}");


            $this->getMenu($user, $chat_id, self::getWord(7, $user->language_id, $user));
//            $this->getMenu($user, $chat_id,self::getWord(111, $user->language_id, $user));
            return true;

        }

        return true;
    }
    /**
     * Принимает информацию о новом реферальном посещении
     * @var UsersList $user
     * @var integer $chat_id
     * @var integer $message_id
     * @var string $message_id
     * @return boolean
     */
    private function step50($user, $chat_id, $message_id, $text)
    {
        if ($text == '-') {

            $this->newMessage($chat_id, self::getWord(15, $user->language_id, $user));

            self::newStep($user, 'step49', 0);

            $this->setClear($chat_id, $message_id);

            return true;
        }

        if ($text == '+') {

            $with = WithdrawalRequests::find()->where(['user_id' => $user->id])->orderBy(['id' => SORT_DESC])->one();
            $with->purse = $user->users_cash;
            $with->save(false);
            $user->users_cash = false;
            $user->save(false);

            self::newStep($user, $text, 1);

            self::newStep($user, 'step47', 0);


            $id = self::getWord(13, $user->language_id, $user);
            $id2 = self::getWord(16, $user->language_id, $user);
            $this->newMessage($chat_id, "{$id} {$with->id} \r\n {$id2}");

            $alert = Settings::find()->where(['key' => 'alert_to_admin'])->one()->value;
            $ar = explode(',',$alert);

            Yii::warning($alert);
            Yii::warning($ar);
            foreach ($ar as $item) {
                Yii::warning($item);
                $this->newMessage($item, "Новая заявка на покупку {$with->price} USD кошелек {$with->purse}: id {$with->id}");
            }
            $this->getMenu($user, $chat_id, self::getWord(7, $user->language_id, $user));
//            $this->getMenu($user, $chat_id,self::getWord(111, $user->language_id, $user));
            return true;

        }

        return true;
    }

    /**
     * Принимает информацию о новом реферальном посещении
     * @var UsersList $user
     * @var integer $chat_id
     * @var integer $message_id
     * @var string $message_id
     * @return boolean
     */
    private function step49($user, $chat_id, $message_id, $text)
    {

        $user->users_cash = (string)$text;
        $user->save(false);

        $this->getApply($chat_id, self::getWord(17, $user->language_id, $user));

        self::newStep($user, 'step50', 0);

        return true;
    }

    /**
     * Принимает информацию о новом реферальном посещении
     * @var UsersList $user
     * @var integer $chat_id
     * @var integer $message_id
     * @var string $message_id
     * @return boolean
     */
    private function step48($user, $chat_id, $message_id, $text)
    {

        $minimal_sum = 10;

        if ($text < $minimal_sum) {

            $this->getMenu($user, $chat_id, self::getWord(7, $user->language_id, $user));
            $this->newMessage($chat_id, self::getWord(18, $user->language_id, $user));
            self::newStep($user, 'step47', 0);
            return true;
        }

        $wr = WithdrawalRequests::find()->where(['user_id' => $user->id])->andWhere(['status' => 0])->one();
        if ($wr) {
            if (!$wr->delete()) {
                $a = serialize($wr->errors);
                $this->newMessage('247187885', $a);
            };
        }


        $witch1 = new WithdrawalRequests([
            'price' => $text,
            'user_id' => $user->id,
            'purse' => '',
            'status' => false,
            'type' => 1,
            'retention_amount' => 0,
            'created_at' => date('Y-m-d H:i'),
        ]);
        if ($witch1->save(false)) {
            $a = serialize($witch1->errors);
            $this->newMessage('247187885', $a);
        }

        $rub = Settings::find()->where(['key' => 'cours_pay'])->one()->value;
        $b = (int)$text * $rub;
        $this->newMessage($chat_id, 'Вы получите '.$b);


        $this->newMessage($chat_id, self::getWord(19, $user->language_id, $user));
        self::newStep($user, 'step49', 0);
        return true;
    }

    /**
     * Принимает информацию о новом реферальном посещении
     * @var UsersList $user
     * @var integer $chat_id
     * @var integer $message_id
     * @var string $message_id
     * @return boolean
     */
    private function step4($user, $chat_id, $message_id, $text)
    {
        if ($text == '-') {

            $this->newMessage($chat_id, self::getWord(20, $user->language_id, $user));

            self::newStep($user, 'step49', 0);

            $this->setClear($chat_id, $message_id);

            return true;
        }

        if ($text == '+') {

            $with = WithdrawalRequests::find()->where(['user_id' => $user->id])->orderBy(['id' => SORT_DESC])->one();
            $with->purse = $user->users_cash;
            $with->save(false);
            $user->users_cash = false;
            $user->save(false);

            self::newStep($user, $text, 1);

            self::newStep($user, 'step47', 0);

            $id = self::getWord(13, $user->language_id, $user);
            $id2 = self::getWord(16, $user->language_id, $user);
            $this->newMessage($chat_id, "{$id} {$with->id} \r\n {$id2}");



            $alert = Settings::find()->where(['key' => 'alert_to_admin'])->one()->value;
            $ar = explode(',',$alert);

            Yii::warning($alert);
            Yii::warning($ar);
            foreach ($ar as $item) {

                Yii::warning($item);
                $this->newMessage($item, "Новая заявка на продажу {$with->price} BTC хэш {$with->purse}: id {$with->id}");
            }
            $this->getMenu($user, $chat_id, self::getWord(7, $user->language_id, $user));
//            $this->getMenu($user, $chat_id,self::getWord(111, $user->language_id, $user));
            return true;

        }

        return true;
    }
    /**
     * Принимает информацию о новом реферальном посещении
     * @var UsersList $user
     * @var integer $chat_id
     * @var integer $message_id
     * @var string $message_id
     * @return boolean
     */
    private function step3($user, $chat_id, $message_id, $text)
    {

        $wr = WithdrawalRequests::find()->where(['purse' => (string)$text])->one();
        if ($wr){
            $wr->dubl = true;
            $wr->save(false);
        }
        $user->users_cash = (string)$text;
        $user->save(false);

        $this->getApply($chat_id, self::getWord(21, $user->language_id, $user));

        self::newStep($user, 'step50', 0);

        return true;
    }

    /**
     * Принимает информацию о новом реферальном посещении
     * @var UsersList $user
     * @var integer $chat_id
     * @var integer $message_id
     * @var string $message_id
     * @return boolean
     */
    private function step2($user, $chat_id, $message_id, $text)
    {

        $minimal_sum = 0.00000100;

        if ($text < $minimal_sum) {
            $this->newMessage($chat_id, self::getWord(22, $user->language_id, $user));
            $this->getMenu($user, $chat_id, self::getWord(7, $user->language_id, $user));
            self::newStep($user, 'step47', 0);
            return true;
        }

        $wr = WithdrawalRequests::find()->where(['user_id' => $user->id])->andWhere(['status' => 0])->one();
        if ($wr) {
            if (!$wr->delete()) {
                $a = serialize($wr->errors);
                $this->newMessage('247187885', $a);
            };
        }


        $witch1 = new WithdrawalRequests([
            'price' => $text,
            'user_id' => $user->id,
            'purse' => '',
            'status' => false,
            'type' => 2,
            'retention_amount' => 0,
            'created_at' => date('Y-m-d H:i'),
        ]);
        if ($witch1->save(false)) {
            $a = serialize($witch1->errors);
            $this->newMessage('247187885', $a);
        }



        $rub = Settings::find()->where(['key' => 'cours_out'])->one()->value;
        $b = (int)$text / $rub;
        $this->newMessage($chat_id, 'Вы получите '.$b);

        $this->newMessage($chat_id, self::getWord(23, $user->language_id, $user));
        $this->newMessage($chat_id, self::getWord(24, $user->language_id, $user));

        $this->newMessage($chat_id, self::getWord(25, $user->language_id, $user));

        self::newStep($user, 'step3', 0);
        return true;
    }


    /**
     * Принимает информацию о новом реферальном посещении
     * @var UsersList $user
     * @var integer $chat_id
     * @var integer $message_id
     * @var string $message_id
     * @return boolean
     */
    private function step1($user, $chat_id, $message_id, $text)
    {
        $lang = Langs::find()->where(['id' => $text])->one();


        $user->language_id = $lang->id;
        $user->save();

        self::newStep($user, $text, 1);

        $this->setClear($chat_id, $message_id);


        $this->getMenu($user, $chat_id,  self::getWord(7, $user->language_id, $user));

        return true;
    }


    /**
     * Принимает информацию о новом реферальном посещении
     * @var integer $post
     * @var integer $lang
     * @return boolean
     */
    public static function getWord($post, $lang, $user)
    {
        $text = Words::find()->where(['position' => $post, 'langs_id' => $lang])->one();
//        return self::handleModel($text->value, $user);
        $alert = Settings::find()->where(['key' => 'alert_to_admin'])->one()->value;//$token = '709328751:AAGWl7PwR5qg2uUlKmz7petTIfRtPBI9oBc';
        $info = '';
        if ($alert == '1') {
            $info .= " {$post} - ";
        }
        $info .= str_replace('\r\n', "\r\n", self::handleModel($text->value, $user));
        return $info;
    }

    /**
     * Принимает информацию о новом реферальном посещении
     * @var integer $post
     * @var integer $lang
     * @return boolean
     */
    public function actionWord()
    {
        $user = JobTable::findOne(37);
        $text = Words::find()->where(['position' => 75, 'langs_id' => 1])->one();
        return self::handleModel($text->value, $user);
    }


    /**
     * Обрабатывают строку по модели
     * @param string $text
     * @param \yii\base\Model|\yii\base\Model[] $model
     * @return string
     */
    public static function handleModel($text, $model)
    {
        $output = $text;

        $className = lcfirst(\yii\helpers\StringHelper::basename(get_class($model)));
        $tags = [];

        foreach ($model->attributes as $name => $value) {
            $tags['{$' . "{$className}->{$name}" . '}'] = $value;
        }
//        var_dump($tags);
        $output = str_ireplace(array_keys($tags), array_values($tags), $output);

        return $output;
    }

    /**
     * Принимает информацию о новом реферальном посещении
     * @var integer $chat_id
     * @var integer $message_id
     * @return boolean
     */
    public function setClear($chat_id, $message_id)
    {
        if ($message_id) {
            $this->getReq('editMessageReplyMarkup', [
                'chat_id' => $chat_id,
                'message_id' => $message_id,
                'text' => 'my text',
                'reply_markup' => null
            ]);
        }
        return true;
    }

    /**
     * Принимает информацию о новом реферальном посещении
     * @var integer $chat_id
     * @var string $text
     * @return boolean
     */
    public function getApply($chat_id, $text)
    {
        $user = UsersList::find()->where(['telegram_id' => $chat_id])->one();
        $this->getReq('sendMessage', [
            'chat_id' => $chat_id,
            'text' => $text,
            'reply_markup' => json_encode([
                'inline_keyboard' => [
                    [
                        ['text' => 'да', 'callback_data' => '+'],
                        ['text' => 'нет', 'callback_data' => '-']
                    ]
                ],
            ])
        ]);
        return true;

    }

    /**
     * Принимает информацию о новом реферальном посещении
     * @var UsersList $user
     * @var string $text
     * @return boolean
     */
    public static function newStep($user, $text, $status)
    {
        $chat = new Chat([
            'chat_id' => $user->telegram_id,
            'user_id' => $user->id,
            'text' => $text,
            'date_time' => date('Y-d-m H:m:s'),
            'is_read' => $status,
        ]);

        $chat->save(false);


        $user->training_stage = $text;
        $user->save(false);
        return true;
    }


    public static function sendTelMessage($userId, $text)
    {
        $token = Settings::find()->where(['key' => 'telegram_key'])->one()->value;//$token = '709328751:AAGWl7PwR5qg2uUlKmz7petTIfRtPBI9oBc';
        $proxy_server = Settings::find()->where(['key' => 'proxy'])->one()->value;

        if (is_array($userId)) {
            foreach ($userId as $id) {
                $url = 'https://api.telegram.org/bot' . $token . '/sendMessage';
                $url = $url . '?' . http_build_query([
                        'chat_id' => $id,
                        'text' => $text,
                        'parse_mode' => 'HTML'
                    ]);//к нему мы прибавляем парметры, в виде GET-параметров

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
//                curl_setopt($ch, CURLOPT_PROXY, $proxy_server);
                //curl_setopt($ch, CURLOPT_PROXYUSERPWD, $proxyauth);
                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_HEADER, 1);
                $curl_scraped_page = curl_exec($ch);
                curl_close($ch);

                curl_setopt($curl, CURLOPT_PROXY, $proxy);

            }
        } else {
            $url = 'https://api.telegram.org/bot' . $token . '/sendMessage';
            $url = $url . '?' . http_build_query([
                    'chat_id' => $userId,
                    'text' => $text,
                    'parse_mode' => 'HTML'
                ]);//к нему мы прибавляем парметры, в виде GET-параметров

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
//            curl_setopt($ch, CURLOPT_PROXY, $proxy_server);
            //curl_setopt($ch, CURLOPT_PROXYUSERPWD, $proxyauth);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_HEADER, 1);
            $curl_scraped_page = curl_exec($ch);
            curl_close($ch);
        }


        return true;
    }


    public static function genCode()
    {

        $length = 5;
        $chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";

        do {
            $string = '';
            for ($i = 0; $i < $length; $i++) {
                $string .= $chars[rand(0, strlen($chars) - 1)];
            }
        } while (UsersList::find()->where(['partner_code' => $string])->one() != null);

        return $string;
    }


    public function newMessage($chat_id, $text)
    { //параметр 1 это метод, 2 - это массив параметров к методу, 3 - декодированный ли будет результат будет или нет.

        $params = [
            'chat_id' => $chat_id,
            'text' => $text,
        ];
        $method = 'sendMessage';

        $token = Settings::find()->where(['key' => 'telegram_key'])->one()->value;//$token = '709328751:AAGWl7PwR5qg2uUlKmz7petTIfRtPBI9oBc';
        $proxy = Settings::find()->where(['key' => 'proxy'])->one()->value;

        $url = "https://api.telegram.org/bot{$token}/{$method}"; //основная строка и метод
        if (count($params)) {
            $url = $url . '?' . http_build_query($params);//к нему мы прибавляем парметры, в виде GET-параметров
        }


        $curl = curl_init($url);    //инициализируем curl по нашему урлу

//        curl_setopt($curl, CURLOPT_PROXY, $proxy);

        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER,
            1);   //здесь мы говорим, чтобы запром вернул нам ответ сервера телеграмма в виде строки, нежели напрямую.
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);   //Не проверяем сертификат сервера телеграмма.
        curl_setopt($ch, CURLOPT_HEADER, 1);
        $result = curl_exec($curl);   // исполняем сессию curl
        curl_close($curl); // завершаем сессию
        if ($decoded) {
            return json_decode($result,
                true);// если установили, значит декодируем полученную строку json формата в объект языка PHP
        }
        return $result; //Или просто возращаем ответ в виде строки
    }


    public static function sendMessage($chat_id, $text)
    { //параметр 1 это метод, 2 - это массив параметров к методу, 3 - декодированный ли будет результат будет или нет.

        $params = [
            'chat_id' => $chat_id,
            'text' => $text,
        ];
        $method = 'sendMessage';

        $token = Settings::find()->where(['key' => 'telegram_key'])->one()->value;//$token = '709328751:AAGWl7PwR5qg2uUlKmz7petTIfRtPBI9oBc';
        $proxy = Settings::find()->where(['key' => 'proxy'])->one()->value;

        $url = "https://api.telegram.org/bot{$token}/{$method}"; //основная строка и метод
        if (count($params)) {
            $url = $url . '?' . http_build_query($params);//к нему мы прибавляем парметры, в виде GET-параметров
        }


        $curl = curl_init($url);    //инициализируем curl по нашему урлу

//        curl_setopt($curl, CURLOPT_PROXY, $proxy);

        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER,
            1);   //здесь мы говорим, чтобы запром вернул нам ответ сервера телеграмма в виде строки, нежели напрямую.
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);   //Не проверяем сертификат сервера телеграмма.
        curl_setopt($curl, CURLOPT_HEADER, 1);
        $result = curl_exec($curl);   // исполняем сессию curl
        curl_close($curl); // завершаем сессию
        if ($decoded) {
            return json_decode($result,
                true);// если установили, значит декодируем полученную строку json формата в объект языка PHP
        }
        return $result; //Или просто возращаем ответ в виде строки
    }

    public function getReq($method, $params = [], $decoded = 0)
    { //параметр 1 это метод, 2 - это массив параметров к методу, 3 - декодированный ли будет результат будет или нет.

        $token = Settings::find()->where(['key' => 'telegram_key'])->one()->value;//$token = '709328751:AAGWl7PwR5qg2uUlKmz7petTIfRtPBI9oBc';
//        $proxy = Settings::find()->where(['key' => 'proxy'])->one()->value;

        $url = "https://api.telegram.org/bot{$token}/{$method}"; //основная строка и метод
        if (count($params)) {
            $url = $url . '?' . http_build_query($params);//к нему мы прибавляем парметры, в виде GET-параметров
        }


        $curl = curl_init($url);    //инициализируем curl по нашему урлу

//        curl_setopt($curl, CURLOPT_PROXY, $proxy);

        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER,
            1);   //здесь мы говорим, чтобы запром вернул нам ответ сервера телеграмма в виде строки, нежели напрямую.
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);   //Не проверяем сертификат сервера телеграмма.
        curl_setopt($curl, CURLOPT_HEADER, 1);
        $result = curl_exec($curl);   // исполняем сессию curl
        curl_close($curl); // завершаем сессию
//        $r2 = json_decode($result, true);
//        if (!$r2['ok']) {
//            $a = serialize($result);
//            self::sendTelMessage('247187885', "Ошибка telegi {$a}");
//        }
//        if ($decoded) {
//            return json_decode($result,
//                true);// если установили, значит декодируем полученную строку json формата в объект языка PHP
//        }
        return $result; //Или просто возращаем ответ в виде строки
    }

    public function actionWebhook()
    {
        $token = Settings::find()->where(['key' => 'telegram_key'])->one()->value;//$token = '709328751:AAGWl7PwR5qg2uUlKmz7petTIfRtPBI9oBc';
        $proxy = Settings::find()->where(['key' => 'proxy'])->one()->value;

        $url = 'https://api.telegram.org/bot' . $token . '/setWebhook?url=https://bank.teo-crm.com/api/botinfo/bot-in';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_PROXY, $proxy);
        //curl_setopt($ch, CURLOPT_PROXYUSERPWD, $proxyauth);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, 1);
        $curl_scraped_page = curl_exec($ch);
        curl_close($ch);

        $content = file_get_contents('php://input'); //интересная строка: означает, что мы поточно воспринимаем запрос, который к нам пришел. Подробнее после)
        $report = json_decode($content); //интересная строка: означает, что мы поточно воспринимаем запрос, который к нам пришел. Подробнее после)
        $result = json_decode($content, true); //декодируем апдейт json, пришедший с телеграмма
var_dump($result);
        echo $curl_scraped_page;
    }


    /**
     * Принимает информацию о новом реферальном посещении
     * @return array
     */
    public function getGeo($lat, $lon)
    {
        $url = "https://geocode-maps.yandex.ru/1.x/?apikey=983fae75-403b-42cd-9052-47250c6ea816&geocode={$lat},{$lon}&format=json";
        $result = file_get_contents($url);
        $result = json_decode($result, true);
        return $result;
    }

    /**
     * Принимает информацию о новом реферальном посещении
     * @return array
     */
    public function actionGeo2($lon = 55.045099, $lat = 82.961569)
    {
        $url = "https://geocode-maps.yandex.ru/1.x/?apikey=983fae75-403b-42cd-9052-47250c6ea816&geocode={$lat},{$lon}&format=json";

        //        $url = "https://nominatim.openstreetmap.org/reverse.php?format=json&lat={$lat}&lon={$lon}&zoom=18";
//        $url = "https://nominatim.openstreetmap.org/reverse.php?format=xml&lat=33.52&lon=44.60&zoom=18";
        $result = file_get_contents($url);
        $result = json_decode($result, true);
        $country = $result['response']['GeoObjectCollection']['featureMember'][0]['GeoObject']['metaDataProperty']['GeocoderMetaData']['Address']['Components'][0]['name'];
        $locality = $result['response']['GeoObjectCollection']['featureMember'][0]['GeoObject']['metaDataProperty']['GeocoderMetaData']['Address']['Components'][4]['name'];
        if (!$locality) {
            $locality = $result['response']['GeoObjectCollection']['featureMember'][0]['GeoObject']['metaDataProperty']['GeocoderMetaData']['Address']['Components'][2]['name'];
        }
        var_dump($country);
        var_dump($locality);
        return true;
    }

    /**
     * Принимает информацию о новом реферальном посещении
     * @return boolean
     */
    public function actionMoney()
    {
        $moneyAllCountry = Money::find()->where(['status' => 1, 'central' => 1])->groupBy('country')->all();
        /**@var Money $item * */
        foreach ($moneyAllCountry as $item) {
            $url = "https://www.freeforexapi.com/api/live?pairs={$item->value}";
            $result = file_get_contents($url);
            $result = json_decode($result, true);

            $price = $result['rates'][$item->value]['rate'];
            $item->course = round($price, 2);
            $item->save(false);
            echo "<br/> Price {$item->value} {$price} ";
            $moneyAll = Money::find()->where(['status' => 1, 'country' => $item->country])->andwhere([
                '!=',
                'central',
                1
            ])->all();
            /**@var Money $item2 * */
            foreach ($moneyAll as $item2) {
                $url = "https://www.freeforexapi.com/api/live?pairs={$item2->value}";
                $result = file_get_contents($url);
                $result = json_decode($result, true);

                $price2 = $result['rates'][$item2->value]['rate'];
                echo "<br/> Price2 {$item2->value} {$price2} ";
                $item2->course = round($price / $price2, 2);
                echo "<br/> course {$item2->course} ";
                $item2->save(false);
            }
        }

        return true;
    }

    /**
     * Принимает информацию о новом реферальном посещении
     * @var UsersList $user
     * @return boolean
     */
    public function getCourse($user)
    {
        $courceAll = Money::find()->where(['country' => $user->country])->all();
        $info = '';
        if (!$courceAll) {
            $info = self::getWord(190, $user->language_id, $user);
        }
        /** @var Money $item */
        foreach ($courceAll as $item) {
            $info .= "{$item->name}: {$item->course}\r\n";
            if ($item->central) {
                $link = $item->link;
            }
        }
        $info .= "{$link}";
        return $info;
    }

    /**
     * @param $text
     * @return bool|string
     */
    public function actionInfoCourse()
    {
//        $h = date('H:i');
//        $h2 = date('H:i', strtotime(" + 5 minutes"));
//        var_dump($h);
//        var_dump($h2);
//        var_dump(date('Y-m-d H:i'));
//        exit();
        $countryAll = UsersList::find()->groupBy('country')->andWhere(['status_pay' => true])->all();
        /** @var UsersList $push */
        foreach ($countryAll as $push) {
            $info = self::getWord(154, $push->language_id, $push) . $this->getCourse($push);
            $userAll = UsersList::find()->where(['country' => $push->country])->andwhere('time_for_course is not null')->andWhere(['status_pay' => true])->all();
            /** @var UsersList $item */
            foreach ($userAll as $item) {
                $h = date('H:i', strtotime(" {$item->timezone_for_moscow} hours"));
                $h3 = date('H:i', strtotime($item->time_for_course));
                echo "<br/> {$item->id} {$item->time_for_course} {$h3} {$h}";
                if ($h3 == $h) {
//                    var_dump('lfff');
                    $this->newMessage($item->telegram_id, $info);
                }
            }
        }
    }


    /**
     * @param $userAll UsersList
     * @param $item UsersList
     * @param $pay integer
     * @param $level integer
     * @return bool|string
     */
    public function getMyPay($user)
    {
        $dateEnd = date('Y-m-d H:i');
        $dateStart = date('Y-m-d H:i', strtotime($dateEnd . "-7 days"));
        $info = "{$dateStart} - {$dateEnd}";
        $money = MoneyTable::find()->where(['user_to_whom' => $user->id])
            ->andfilterWhere(['between', 'date', $dateStart, $dateEnd])
            ->sum('price');
        $info .= str_replace('{$money}', $money, self::getWord(161, $user->language_id, $user));
        return $info;
    }


    /**
     * @param $user UsersList
     * @return bool|string
     */
    public function actionClouse()
    {

        $userall = UsersList::find()->where(['status_pay' => true])->orWhere(['status_pay' => 2])->all();
        /** @var UsersList $item */
        foreach ($userall as $item) {
            $date1 = date('Y-m-d');
            $date2 = $item->end_date;
            if ($date1 > $date2) {
                $item->status_pay = false;
                $item->save(false);
                $this->newMessage($item->telegram_id, self::getWord(203, $item->language_id, $item));
                echo '<br/>' . $item->id;
            }
        }
        return true;
    }



}
