<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "users_list".
 *
 * @property int $id
 * @property string $name Имя
 * @property int $gender Пол
 * @property int $language_id Язык
 * @property string $telegram_id Телеграм ид
 * @property string $avatar_bot Аватар бота
 * @property int $bot_gender Пол бота
 * @property string $bot_name Имя бота
 * @property string $who_invited Кто пригласил
 * @property double $balance Баланс
 * @property string $city Город
 * @property string $country Страна
 * @property int $timezone_for_moscow Часавой пояс формате разница от МСК
 * @property int $motivashka_id Ид последнего просмотра мотивашек
 * @property string $time_for_weather Установить время получения прогноза погоды
 * @property string $time_for_course Установить время получения сводки курса валют
 * @property string $users_cash Кошелок пользователя
 * @property string $send_time_weather Время отправки погоды
 * @property string $course_time Время курса валют
 * @property string $partner_code Код партнерки
 * @property string $end_date Дата завершения подписки
 * @property double $retention_percent Процент удержания
 * @property string $certificate_link Ссылка на удостворение
 * @property string $passport_link Ссылка на паспорт
 * @property int $access_passport Подверждение паспортных данных
 * @property string $fio ФИО
 * @property string $inn ИНН
 * @property int $status_pay Статус платной подписки
 * @property int $finish Статус платной подписки
 * @property int $noti_user Статус платной подписки
 * @property int $noti_money Статус платной подписки
 * @property string $currency_course
 * @property string $training_stage Этап обучения
 * @property string $reminder_morning Время напоминания целей утро
 * @property string $reminder_night Время напоминания целей вечер
 * @property string $send_time_video Время отправки видео
 * @property string $lon lon
 * @property string $lat lat
 * @property string $create_at create_at
 * @property string $pay_subscribe_date Дата покупки подписки
 *
 * @property Chat[] $chats
 * @property JobTable[] $jobTables
 * @property MoneyTable[] $moneyTables
 * @property MoneyTable[] $moneyTables0
 * @property TableWithTargets[] $tableWithTargets
 * @property Langs $language
 * @property VideoMotivashki $motivashka
 * @property WithdrawalRequests[] $withdrawalRequests
 */
class UsersList extends ActiveRecord
{

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'users_list';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [
                [
                    'gender',
                    'language_id',
                    'bot_gender',
                    'timezone_for_moscow',
                    'motivashka_id',
                    'access_passport',
                    'status_pay'
                ],
                'integer'
            ],
            [['balance', 'retention_percent'], 'number'],
            [
                [
                    'time_for_weather',
                    'finish',
                    'time_for_course',
                    'send_time_weather',
                    'course_time',
                    'end_date',
                    'reminder_morning',
                    'reminder_night',
                    'send_time_video',
                    'pay_subscribe_date',
                ],
                'safe'
            ],
            [
                [
                    'name',
                    'telegram_id',
                    'avatar_bot',
                    'bot_name',
                    'who_invited',
                    'city',
                    'country',
                    'users_cash',
                    'partner_code',
                    'certificate_link',
                    'passport_link',
                    'fio',
                    'inn',
                    'currency_course',
                    'training_stage'
                ],
                'string',
                'max' => 255
            ],
            [
                ['language_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Langs::className(),
                'targetAttribute' => ['language_id' => 'id']
            ],
            [
                ['motivashka_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => VideoMotivashki::className(),
                'targetAttribute' => ['motivashka_id' => 'id']
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Имя',
            'gender' => 'Пол',
            'language_id' => 'Язык',
            'telegram_id' => 'Телеграм ид',
            'avatar_bot' => 'Аватар бота',
            'bot_gender' => 'Пол бота',
            'bot_name' => 'Имя бота',
            'who_invited' => 'Кто пригласил',
            'balance' => 'Баланс',
            'city' => 'Город',
            'country' => 'Страна',
            'timezone_for_moscow' => 'Часавой пояс формате разница от МСК',
            'motivashka_id' => 'Ид последнего просмотра мотивашек',
            'time_for_weather' => 'Установить время получения прогноза погоды',
            'time_for_course' => 'Установить время получения сводки курса валют',
            'users_cash' => 'Кошелок пользователя',
            'send_time_weather' => 'Время отправки погоды',
            'course_time' => 'Время курса валют',
            'partner_code' => 'Код партнерки',
            'end_date' => 'Дата завершения подписки',
            'retention_percent' => 'Процент удержания',
            'certificate_link' => 'Ссылка на удостворение',
            'passport_link' => 'Ссылка на паспорт',
            'access_passport' => 'Подверждение паспортных данных',
            'fio' => 'ФИО',
            'inn' => 'ИНН',
            'currency_course' => 'Курс валют',
            'status_pay' => 'Статус платной подписки',
            'training_stage' => 'Этап обучения',
            'reminder_morning' => 'Время напоминания целей утро',
            'reminder_night' => 'Время напоминания целей вечер',
            'send_time_video' => 'Время отправки видео',
            'lon' => 'lon',
            'lat' => 'lat',
            'finish' => 'finish',
            'noti_user' => 'noti_user',
            'noti_money' => 'noti_money',
            'create_at' => 'Дата регистрации',
        ];
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            $this->create_at = date('Y-m-d H:i:s');
            if ($this->end_date){
                $this->pay_subscribe_date = date('Y-m-d H:i:s', time());
            }
        }

        return parent::beforeSave($insert);
    }

    /**
     * @return bool
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function beforeDelete()
    {
        $chat = Chat::find()->where(['user_id' => $this->id])->all();
        foreach ($chat as $value) {
            $value->delete();
        }

        $job_table = JobTable::find()->where(['user_id' => $this->id])->all();
        foreach ($job_table as $value) {
            $value->delete();
        }

        $money_table = MoneyTable::find()->where(['user_from' => $this->id])->all();
        foreach ($money_table as $value) {
            $value->delete();
        }

        $money_table = MoneyTable::find()->where(['user_to_whom' => $this->id])->all();
        foreach ($money_table as $value) {
            $value->delete();
        }

        $target = TableWithTargets::find()->where(['user_id' => $this->id])->all();
        foreach ($target as $value) {
            $value->delete();
        }

        $request = WithdrawalRequests::find()->where(['user_id' => $this->id])->all();
        foreach ($request as $value) {
            $value->delete();
        }

        return parent::beforeDelete();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChats()
    {
        return $this->hasMany(Chat::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJobTables()
    {
        return $this->hasMany(JobTable::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMoneyTables()
    {
        return $this->hasMany(MoneyTable::className(), ['user_from' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMoneyTables0()
    {
        return $this->hasMany(MoneyTable::className(), ['user_to_whom' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTableWithTargets()
    {
        return $this->hasMany(TableWithTargets::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguage()
    {
        return $this->hasOne(Langs::className(), ['id' => 'language_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMotivashka()
    {
        return $this->hasOne(VideoMotivashki::className(), ['id' => 'motivashka_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWithdrawalRequests()
    {
        return $this->hasMany(WithdrawalRequests::className(), ['user_id' => 'id']);
    }

    public function getLanguagesList()
    {
        $lang = Langs::find()->all();
        return ArrayHelper::map($lang, 'id', 'name');
    }

    public function motivationList()
    {
        $video = VideoMotivashki::find()->all();
        return ArrayHelper::map($video, 'id', 'link_youtube');
    }

    public function getGenderList()
    {
        return [
            0 => 'Ж',
            1 => 'М',
        ];
    }

    public function getStatusList()
    {
        return [
            0 => 'Вкл',
            1 => 'Выкл',
        ];
    }

    /**
     * Получает информацию по регистрации (для dashboard)
     * @param string $start_date Начало периода для выборки
     * @param string $end_date Конец периода для выборки
     * @return array
     * [
     * 'count' => 'Общее кол-во зарегистрированных',
     * 'period' => [
     *          'start' => 'Начало периода',
     *          'end' => 'Конец периода',
     *          'count' => 'Количиество зарегистрировавшихся за период',
     *      ]
     * ]
     */
    public static function getRegisteredInfo($start_date, $end_date)
    {
        $count = UsersList::find()->count() ?? 0;
        $period_count = UsersList::find()->andWhere(['BETWEEN', 'create_at', $start_date, $end_date])->count() ?? 0;
        return [
            'count' => $count,
            'period' => [
                'start' => Yii::$app->formatter->format($start_date, 'date'),
                'end' => Yii::$app->formatter->format($end_date, 'date'),
                'count' => $period_count
            ]
        ];
    }

    /**
     * Получает информацию по подпискам (для dashboard)
     * @param string $start_date Начало периода для выборки
     * @param string $end_date Конец периода для выборки
     * @return array
     *  [
     * 'active' => 'Количество пользователей с активной подпиской',
     * 'purchased' => [
     *              'start' => 'Начало периода',
     *              'end' => 'Конец периода',
     *              'count' => 'Кол-во человек, купивших подписку за период',
     *         ]
     * ]
     */
    public static function getSubscribedInfo($start_date, $end_date)
    {
//        $active_subscribe = UsersList::find()->andWhere(['>=', 'end_date', date('Y-m-d', time())])->count() ?? 0;
        $active_subscribe = UsersList::find()->andWhere(['status_pay' => 1])->count() ?? 0;


        return [
            'active' => $active_subscribe,
            'purchased' => [
                'start' => Yii::$app->formatter->format($start_date, 'date'),
                'end' => Yii::$app->formatter->format($end_date, 'date'),
//                'count' => 8
            ]
        ];
    }

    /**
     * Получает информацию по платежам (для dashboard)
     * @return array
     *  [
     *    'received' => [
     *          'all' => общая сумма платежей конвертированная в рубли
     *          'Валюта' => общая суммма по валюте
     *          'Валюта' => общая суммма по валюте
     *      ],
     *    'payable' => 'Подлежит выплате по МЛМ'
     * ]
     */
    public static function getAmountInfo()
    {
        $payed = Pay::find()->distinct('currency')->all();

        $received = [];

        /** @var Pay $pay */
        foreach ($payed as $pay){
            Yii::info($pay->currency, 'test');
            $received[$pay->currency] = round(Pay::find()->andWhere(['currency' => $pay->currency])->sum('amount') ?? 0, 2);
        }

        $received['all'] = round(Pay::find()->sum('amount')?? 0, 2);
        $payable = self::getPayWeek();

        return [
            'received' => $received,
            'payable' => round($payable, 2),
        ];
    }

    /**
     * Рассчитывает суммы выплат по МЛМ всем пользователям
     * @return float
     */
    private static function getPayWeek()
    {
        $payable = 0;
        $userAll = UsersList::find()->all();
        $dateEnd = date('Y-m-d H:i', time());
        $dateStart = date('Y-m-d H:i', strtotime($dateEnd . "-7 days"));
        Yii::info("Интервал {$dateStart} - {$dateEnd}", 'test');
        $sumWeek = Settings::find()->where(['key' => 'summa_daily'])->one()->value;
        $percentWeek = Settings::find()->where(['key' => 'prosent_daily'])->one()->value;
        foreach ($userAll as $user) {
            $money = MoneyTable::find()->where(['user_to_whom' => $user->id])
                ->andfilterWhere(['between', 'date', $dateStart, $dateEnd])
                ->sum('price');
            if ($money >= $sumWeek) {
                $payable += round(($money / 100) * $percentWeek);
            }
        }

        return round($payable, 2);
    }
}
