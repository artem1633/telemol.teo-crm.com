<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "chat".
 *
 * @property int $id
 * @property string $chat_id
 * @property int $user_id
 * @property string $text
 * @property string $date_time
 * @property int $is_read
 *
 * @property UsersList $user
 */
class Chat extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'chat';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'is_read'], 'integer'],
            [['text'], 'string'],
            [['date_time'], 'safe'],
            [['chat_id'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => UsersList::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'chat_id' => 'Chat ID',
            'user_id' => 'User ID',
            'text' => 'Text',
            'date_time' => 'Date Time',
            'is_read' => 'Is Read',
        ];
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            //$this->user_id = Yii::$app->user->identity->id;
            $this->date_time = date('Y-m-d H:i:s');
            $this->is_read = false;
        }

        return parent::beforeSave($insert);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(UsersList::className(), ['id' => 'user_id']);
    }
}
