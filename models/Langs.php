<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "langs".
 *
 * @property int $id
 * @property string $name Наименование
 * @property string $lang yanex
 * @property int $status Статус
 *
 * @property UsersList[] $usersLists
 * @property Words[] $words
 */

class Langs extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'langs';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['status'], 'integer'],
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
            [['lang'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
            'lang' => 'Для яндекса',
            'status' => 'Статус',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsersLists()
    {
        return $this->hasMany(UsersList::className(), ['language_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWords()
    {
        return $this->hasMany(Words::className(), ['langs_id' => 'id']);
    }

    public function getStatusList()
    {
        return [
            0 => 'Выкл',
            1 => 'Вкл',
        ];
    }
}
