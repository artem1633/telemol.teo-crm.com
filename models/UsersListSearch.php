<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\UsersList;

/**
 * UsersListSearch represents the model behind the search form about `app\models\UsersList`.
 */
class UsersListSearch extends UsersList
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'gender', 'language_id', 'bot_gender', 'timezone_for_moscow', 'motivashka_id', 'access_passport', 'status_pay'], 'integer'],
            [['name', 'telegram_id', 'avatar_bot', 'bot_name', 'who_invited', 'city', 'country', 'time_for_weather', 'time_for_course', 'users_cash', 'send_time_weather', 'course_time', 'partner_code', 'end_date', 'certificate_link', 'passport_link', 'fio', 'inn', 'currency_course', 'training_stage', 'reminder_morning', 'reminder_night', 'send_time_video'], 'safe'],
            [['balance', 'retention_percent'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = UsersList::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'gender' => $this->gender,
            'language_id' => $this->language_id,
            'bot_gender' => $this->bot_gender,
            'balance' => $this->balance,
            'timezone_for_moscow' => $this->timezone_for_moscow,
            'motivashka_id' => $this->motivashka_id,
            'time_for_weather' => $this->time_for_weather,
            'time_for_course' => $this->time_for_course,
            'send_time_weather' => $this->send_time_weather,
            'course_time' => $this->course_time,
            'end_date' => $this->end_date,
            'retention_percent' => $this->retention_percent,
            'access_passport' => $this->access_passport,
            'currency_course' => $this->currency_course,
            'status_pay' => $this->status_pay,
            'training_stage' => $this->training_stage,
            'reminder_morning' => $this->reminder_morning,
            'reminder_night' => $this->reminder_night,
            'send_time_video' => $this->send_time_video,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'telegram_id', $this->telegram_id])
            ->andFilterWhere(['like', 'avatar_bot', $this->avatar_bot])
            ->andFilterWhere(['like', 'bot_name', $this->bot_name])
            ->andFilterWhere(['like', 'who_invited', $this->who_invited])
            ->andFilterWhere(['like', 'city', $this->city])
            ->andFilterWhere(['like', 'country', $this->country])
            ->andFilterWhere(['like', 'users_cash', $this->users_cash])
            ->andFilterWhere(['like', 'partner_code', $this->partner_code])
            ->andFilterWhere(['like', 'certificate_link', $this->certificate_link])
            ->andFilterWhere(['like', 'passport_link', $this->passport_link])
            ->andFilterWhere(['like', 'fio', $this->fio])
            ->andFilterWhere(['like', 'inn', $this->inn]);

        return $dataProvider;
    }
}
