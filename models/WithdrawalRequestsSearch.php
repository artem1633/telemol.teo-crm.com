<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\WithdrawalRequests;

/**
 * WithdrawalRequestsSearch represents the model behind the search form about `app\models\WithdrawalRequests`.
 */
class WithdrawalRequestsSearch extends WithdrawalRequests
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id'], 'integer'],
            [['price', 'retention_amount'], 'number'],
            [['purse', 'created_at', 'date_payment'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = WithdrawalRequests::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'price' => $this->price,
            'user_id' => $this->user_id,
            'retention_amount' => $this->retention_amount,
            'created_at' => $this->created_at,
            'date_payment' => $this->date_payment,
        ]);

        $query->andFilterWhere(['like', 'purse', $this->purse]);

        return $dataProvider;
    }
}
