<?php

namespace app\base;

use yii\db\ActiveQuery;

/**
 * Class AppActiveQuery
 * @package app\models
 * @see \app\models\Clients
 */
class AppActiveQuery extends ActiveQuery
{
    public $userId;
    /**
     * @inheritdoc
     */
    public function all($db = null)
    {
        if($this->userId != null) {
            $tableName = $this->getPrimaryTableName();

            $this->andWhere([$tableName . '.user_id' => $this->userId]);
        }

        return parent::all($db);
    }

    /**
     * @param string $q
     * @param null $db
     * @return array|int|string|\yii\db\ActiveRecord[]
     */
    public function count($q = '*', $db = null)
    {
        if($this->userId != null)
        {
            $tableName = $this->getPrimaryTableName();

            $this->andWhere([$tableName.'.user_id' => $this->userId]);
        }

        return parent::count($q, $db);
    }
}