<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Words */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="words-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'value')->textarea(['rows' => 2]) ?>

    <?= $form->field($model, 'position')->textInput(['type' => 'number']) ?>

    <?= $form->field($model, 'langs_id')->dropDownList($model->getLangsList(),[]) ?>

    <pre style="word-wrap: break-word; white-space: pre-wrap;">-------------------------------------------------------------------
-----------------Подстановка слов------------------------------------

<i style="color: gray;">
Позиция 1 - Инструкция:
Позиция 2 - Курс:
Позиция 3 - введите сумму в usd


</i>

</pre>


    <?php if (!Yii::$app->request->isAjax){ ?>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    <?php } ?>

    <?php ActiveForm::end(); ?>

</div>
