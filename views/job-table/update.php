<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\JobTable */
?>
<div class="job-table-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
