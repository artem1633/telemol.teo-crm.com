<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\UsersList */

?>
<div class="users-list-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
